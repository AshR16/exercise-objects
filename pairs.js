function pairs(obj) {

    let result = []
    if( obj == undefined) return result
    for(let index in obj){
        result.push([index,obj[index]])
    }
    return result
}

module.exports = pairs