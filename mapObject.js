function mapObject(obj,cb){

    let result = {}
    for(let index in obj){
        result[index] = cb(obj[index])
    }
    return result
}

module.exports = mapObject