function defaults(object, defaultProps){

    for(let index in defaultProps){
        if(object[index] === undefined){
            object[index] = defaultProps[index]
        }

    }

    return object

}

module.exports = defaults